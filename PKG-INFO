Metadata-Version: 1.2
Name: python-cpl
Version: 0.7.4
Summary: Python interface for the ESO Common Pipeline Library
Home-page: https://pypi.org/project/python-cpl/0.7.4
Author: Ole Streicher
Author-email: python-cpl@liska.ath.cx
License: GPL
Download-URL: https://files.pythonhosted.org/packages/source/p/python-cpl/python-cpl-0.7.4.tar.gz/python-cpl-0.7.4.tar.gz
Description: This module can list, configure and execute CPL-based recipes from Python
        (python2 and python3).  The input, calibration and output data can be
        specified as FITS files or as ``astropy.io.fits`` objects in memory.
        
        The ESO `Common Pipeline Library <http://www.eso.org/sci/software/cpl/>`_
        (CPL) comprises a set of ISO-C libraries that provide a comprehensive,
        efficient and robust software toolkit. It forms a basis for the creation of
        automated astronomical data-reduction tasks. One of the features provided by
        the CPL is the ability to create data-reduction algorithms that run as plugins
        (dynamic libraries). These are called "recipes" and are one of the main
        aspects of the CPL data-reduction development environment.
        
Platform: UNKNOWN
Classifier: Development Status :: 4 - Beta
Classifier: Intended Audience :: Science/Research
Classifier: License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)
Classifier: Operating System :: MacOS :: MacOS X
Classifier: Operating System :: POSIX
Classifier: Operating System :: Unix
Classifier: Programming Language :: C
Classifier: Programming Language :: Python
Classifier: Programming Language :: Python :: 2
Classifier: Programming Language :: Python :: 3
Classifier: Topic :: Scientific/Engineering :: Astronomy
Provides: cpl
Requires-Python: >=2.7
